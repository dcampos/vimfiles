" Maintainer: Darlan P. de Campos <darlanpedro (a) gmail com>
" License: Public Domain

" INÍCIO {{{

" Pasta do Vim
let s:vim_folder = g:home_prefix . "vim_local/"

exec "set rtp^=" . s:vim_folder . "," . s:vim_folder . 'after,'

" runtime plugin/functions.vim

set nocompatible

" }}}


" VIM-PLUG {{{

call plug#begin(s:vim_folder . "plugged/")

Plug 'liuchengxu/vim-which-key'

Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rsi'

Plug 'scrooloose/nerdtree'
Plug 'w0rp/ale'
Plug 'easymotion/vim-easymotion'
Plug 'vim-perl/vim-perl', { 'for': 'perl', 'do':
   \ 'make clean carp dancer highlight-all-pragmas moose test-more try-tiny' }
Plug 'c9s/perlomni.vim', { 'for': 'perl' }
Plug 'hotchpotch/perldoc-vim', { 'for': 'perl' }
Plug 'vim-perl/vim-perl6'
" Plug 'Yggdroot/indentLine'
Plug 'tyru/open-browser.vim'
Plug 'majutsushi/tagbar'
Plug 'terryma/vim-multiple-cursors'
Plug 'itchyny/lightline.vim'
Plug 'mileszs/ack.vim'

" Completion
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'pgilad/vim-skeletons'
Plug 'ervandew/supertab'
Plug 'mattn/emmet-vim'

Plug 'airblade/vim-rooter'

Plug 'tomtom/tcomment_vim'

" Tipos de arquivos
Plug 'dcampos/proguard.vim', { 'for': 'proguard' }
Plug 'quabug/vim-gdscript'
Plug 'sheerun/vim-polyglot'
Plug 'plasticboy/vim-markdown'

" Plug 'jaxbot/browserlink.vim'

Plug 'junegunn/vim-easy-align'

Plug 'junegunn/vader.vim'

Plug 'lifepillar/vim-solarized8'

" Plug '~/.fzf'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'airblade/vim-gitgutter'

call plug#end()

" END VIM-PLUG }}}


" VARIÁVEIS DE CONFIGURAÇÃO {{{

" Mapleader: configura a tecla <leader>.
let mapleader = ","
let g:mapleader = ","

" TOHTML
" let g:html_use_css=0
let g:html_number_lines = 0

let g:ws_enabled = 1

let g:SuperTabCrMapping = 0
let g:SuperTabDefaultCompletionType = 'context'
let g:SuperTabContextDefaultCompletionType = '<c-n>'

" vim-rooter
let g:rooter_change_directory_for_non_project_files = 1
let g:rooter_silent_chdir = 1
let g:rooter_manual_only = 1

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'

let skeletons#skeletonsDir = s:vim_folder . '/skeletons'

let g:ale_linters = {'perl6': ['perl6']}

let g:indentLine_leadingSpaceEnabled = 0
let g:indentLine_Enabled = 0

let NERDTreeMinimalUI=1
" let NERDTreeDirArrowExpandable = '🖿 '
" let NERDTreeDirArrowCollapsible = '🖿 '

let g:netrw_banner = 0
let g:netrw_winsize = 25
let g:netrw_liststyle = 3

" }}}


" DEFINIÇÕES {{{

" Nenhum aviso visual
set novisualbell
set noerrorbells

set number
set linebreak

set lazyredraw

" Recuo de texto
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab
set autoindent

" Backspace em mais de uma linha
set backspace=eol,start,indent

set nobackup
set noswapfile

set history=500

set ignorecase smartcase

set incsearch

set cursorline

syntax on

filetype plugin on

" Necessary for vim to show status line
set laststatus=2

set noshowmode

" Menu para completar comandos
set wildmenu
set wildignorecase

" Adiciona _ a iskeyword
set iskeyword+=_

" Área de transferência
if has("unnamedplus")
    set clipboard=unnamedplus
else
    set clipboard=unnamed
endif

" Necessário para exibir corretamente todos os caracteres
set encoding=utf8

set fillchars=vert:\ ,stl:\ ,stlnc:\ ,fold:-,diff:-

set listchars=tab:»\ ,eol:¬,trail:·,space:·,nbsp:·,precedes:‹,extends:›

set timeoutlen=500
set updatetime=500
set termguicolors
set mouse=nvi

" }}}


" MAPEAMENTOS {{{

" Leader {{{

let g:which_key_map = {}

" Inserir linhas e continuar em modo normal
nnoremap <Leader>o o<ESC>:echo<CR>
nnoremap <Leader>O O<ESC>:echo<CR>

" vim-fugitive
nnoremap <Leader>gc :Gcommit<CR>
nnoremap <Leader>gs :Gstatus<CR>
nnoremap <Leader>gd :Gdiff<CR>
nnoremap <Leader>gp :Git push<CR>
nnoremap <Leader>gu :Git pull -u<CR>
nnoremap <Leader>gl :Glog<CR>

let g:which_key_map.g = { 'name' : '+git' }

" Espaços em branco
nnoremap <Leader>tw :ToggleWhitespace<CR>
nnoremap <Leader>rw :RemoveWhitespace<CR>
vnoremap <Leader>rw :RemoveWhitespace<CR>

nnoremap <Leader>bn :bn<CR>
nnoremap <Leader>bp :bp<CR>
nnoremap <Leader>bd :bd<CR>
nnoremap <Leader>bl :ls<CR>

let g:which_key_map.b = { 'name' : '+buffer' }

nnoremap <Leader>fs :FZF<CR>
nnoremap <Leader>fw :w<CR>
nnoremap <Leader>fq :q<CR>
nnoremap <Leader>fe :Explore<CR>
nnoremap <Leader>ft :Lexplore<CR>

let g:which_key_map.f = { 'name' : '+file' }

" Janelas
nnoremap <Leader>wh <C-w>h
nnoremap <Leader>wj <C-w>j
nnoremap <Leader>wk <C-w>k
nnoremap <Leader>wl <C-w>l
nnoremap <Leader>ww <C-w>w

let g:which_key_map.w = { 'name' : '+window' }

" FZF
" TODO: decide if I will add <Leader>fs etc. as well
nnoremap <Leader>sa :Ag<CR>
nnoremap <Leader>sb :Buffers<CR>
nnoremap <Leader>sf :FZF<CR>
nnoremap <Leader>sg :GFiles<CR>
nnoremap <Leader>sh :FZF ~<CR>
nnoremap <Leader>st :Tags<CR>
nnoremap <Leader>sw :Windows<CR>

let g:which_key_map.s = { 'name' : '+search' }

" Toggle
" Toggle list
nnoremap <Leader>tl :set list!<CR>
" Toggle background
nnoremap <silent> <Leader>tb :let &bg = (&bg == 'dark' ? 'light' : 'dark')<CR>

" }}}


call which_key#register(',', 'g:which_key_map')

nnoremap <silent> <leader> :WhichKey ','<CR>
vnoremap <silent> <leader> :WhichKeyVisual ','<CR>

noremap <c-p> :FZF<CR>

" Alternar NERDTree
noremap <F2> :NERDTreeToggle<CR>

" Barra de espaço/backspace mapeados para PageDown/PageUp no modo de comandos
noremap <Space> <c-d>
noremap <BS> <c-u>

" Abrir links da web/pesquisar palavras
let g:netrw_nogx = 1 " disable netrw's gx mapping.

nmap gx <Plug>(openbrowser-smart-search)
vmap gx <Plug>(openbrowser-smart-search)
" let g:netrw_browsex_viewer='firefox'

" Navegação por linhas visuais
noremap j gj
noremap k gk

" Correção ortográfica
noremap <Leader>ss :set spell!<cr>

" EasyAlign
vmap <Enter> <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k

" Quicker navigation

nnoremap <silent> ]q :cnext<cr>
nnoremap <silent> [q :cprev<cr>
nnoremap <silent> ]l :lnext<cr>
nnoremap <silent> [l :lprev<cr>
nnoremap <silent> ]b :bnext<cr>
nnoremap <silent> [b :bprev<cr>

" }}}


" ABREVIAÇÕES {{{


" Evitar erros ao salvar/sair
cabbrev W w
cabbrev Q q
cabbrev Wq wq
cabbrev wQ wq
cabbrev WQ wq

" }}}


" DIVERSOS/AUTO-COMANDOS {{{

" Ruby
augroup ft_ruby
    autocmd!
    " Recuo de texto
    autocmd BufNewFile,BufRead *.rb setlocal tabstop=2 shiftwidth=2
augroup end

" Outros tipos
augroup ft_misc
    autocmd!

    " Arquivos md: tratar como markdown
    autocmd BufNewFile,BufRead *.md set ft=markdown
augroup end

" Inserir modelo quando iniciar novo arquivo
augroup templates
    autocmd!
    " read in template files
    autocmd BufNewFile *.* silent! execute '0r ' . s:vim_folder . 'templates/template.'.expand("<afile>:e")

    " parse special text in the templates after the read
    autocmd BufNewFile * %substitute#<+VIM+>\(.\{-\}\)<+END_VIM+>#\=eval(submatch(1))#ge
augroup end

"autocmd BufRead,BufNewFile *.txt set spell
augroup cursor_line
    autocmd!

    " Only highlight cursor line in active buffer window
    autocmd WinLeave * set nocursorline
    autocmd WinEnter * if &filetype == 'qf' | set nocursorline | else | set cursorline | endif
augroup end

augroup nerdtree
    autocmd!
    " autocmd BufEnter NERD_tree* :LeadingSpaceDisable
augroup end

augroup nostatusline
    autocmd! FileType fzf,which_key
    autocmd  FileType fzf,which_key set laststatus=0 noruler
      \| autocmd BufLeave <buffer> set laststatus=2 ruler
augroup end

augroup etcgroup
    autocmd!

    " Omnicomplete
    if exists("+omnifunc")
        autocmd Filetype *
                    \   if &omnifunc == "" |
                    \       setlocal omnifunc=syntaxcomplete#Complete |
                    \   endif
    endif

    " Muda para o diretório do arquivo atual
    " autocmd BufEnter * lcd %:p:h
    autocmd BufEnter * RooterEnter
    autocmd BufLeave * RooterLeave


    " Destacar espaços em branco
    autocmd VimEnter,BufNewFile,BufRead * silent! :SetupWhitespace
    autocmd BufRead,InsertLeave * silent! :ShowWhitespace
    autocmd InsertEnter * silent! :HideWhitespace
augroup end

" }}}


" GUI {{{

" let g:indentLine_char = '˰'

if has("gui_running")
    set guioptions=gAmec

    set bg=dark
    colors solarized8
else
    colors ir_black
endif

" }}}


" vim:fdm=marker
