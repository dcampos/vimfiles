" Vim configuration for Linux

let g:home_prefix = '~/'

exec "source" g:home_prefix . "vim_local/vimrc"

set termguicolors

set mouse=a

set background=dark

colorscheme solarized8_flat

if exists('g:gnvim')
    set guifont=Ubuntu\ Mono:h13
endif

