if !exists('g:loaded_lightline')
  finish
endif

" lightline.vim
let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'fugitive', 'readonly', 'curdir', 'filename', 'modified' ] ],
      \   'right': [ [ 'column' ], [ 'percent' ], [ 'fileencoding' ] ]
      \ },
      \ 'inactive': {
      \   'left': [ [ 'filename' ] ],
      \   'right': [ ]
      \ },
      \ 'component_function': {
      \   'fugitive' : 'LightlineFugitive',
      \   'curdir'   : 'LightlineDir',
      \   'mode'     : 'LightlineMode',
      \   'fileencoding' : 'LightlineFileencoding',
      \ },
      \ 'subseparator': { 'left': '·', 'right': '·' }
  \ }

function! LightlineFugitive()
    if winwidth(0) < 60 | return '' | endif
    if exists("*fugitive#head")
        let _ = fugitive#head()
        return strlen(_) ? '' . _ : ''
    endif
    return ''
endfunction

function! LightlineDir()
    if winwidth(0) < 60 | return '' | endif
    let _path = substitute(getcwd(), '^' . $HOME, '~', '')
    let _spath = split(_path, "/")
    if len(_path) < 25
        return join(_spath, "/")
    else
        return _spath[0] . '/…/' . _spath[-1]
    endif
endfunction

function LightlineFileencoding()
    return (winwidth(0) < 60 || &fileencoding =~? 'utf-8') ? '' : &fileencoding
endfunction

function! LightlineMode()
    let fname = expand('%:t')
    return fname =~ 'NERD_tree' ? 'NERDTree' : lightline#mode()
endfunction

if g:colors_name =~ 'solarized'
    let g:lightline.colorscheme = 'solarized'
endif

